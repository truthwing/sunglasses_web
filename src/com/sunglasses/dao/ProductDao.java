package com.sunglasses.dao;

import java.util.List;

import com.sunglasses.dto.HashDto;
import com.sunglasses.dto.ImageDto;
import com.sunglasses.dto.ProductDto;


public interface ProductDao {
	public List<ProductDto> getProductList();
	public List<HashDto> getHashList(int p_idx);
	public List<ImageDto> getModelList(int p_idx);
	public List<ImageDto> getDesignList(int p_idx);
	public List<ImageDto> getLensList(int p_idx);
	public List<ImageDto> getImageList(int p_idx);
}
