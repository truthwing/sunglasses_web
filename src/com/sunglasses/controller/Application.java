package com.sunglasses.controller;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class Application extends ResourceConfig{

	public Application(){
//		register(SpringService.class);
//		register(UploadService.class);
//		register(CompanyService.class);
		register(DownloadService.class);
		register(ProductService.class);
		register(RequestContextFilter.class);
		register(JacksonFeature.class);
		register(MultiPartFeature.class);
	}


}
