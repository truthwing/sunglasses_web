package com.sunglasses.controller;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.stereotype.Component;

@Component
@Path("download")
public class DownloadService {
//	@GET
//	@Path("image/{file_name}")
//	@Produces(MediaType.APPLICATION_OCTET_STREAM)
//	public Response getFile(@PathParam("file_name") String file_name) {
//        File file = new File(PathToFile.UPLOADED_PATH+file_name);
//        System.out.println("download : "+file.getAbsolutePath());
//	    ResponseBuilder response = Response.ok((Object) file);
//	    response.header("Content-Disposition", "attachment; filename="+file.getName());
//	    return response.build();
//	}
	@GET
	@Path("image/{file_name}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getImage(@PathParam("file_name") String file_name) {
	    File file = new File(PathToFile.UPLOADED_PATH+file_name);
        System.out.println("download : "+file.getAbsolutePath());
	    ResponseBuilder response = Response.ok((Object) file);
	    response.header("Content-Disposition", "attachment; filename="+file.getName());
	    return response.build();
	}
	
	@GET  
    @Path("txt")
    @Produces("text/plain")  
    public Response getTxtFile() {  
        File file = new File(PathToFile.UPLOADED_PATH+"test.txt");  
        System.out.println("download : "+file.getAbsolutePath());
        ResponseBuilder response = Response.ok((Object) file);  
        response.header("Content-Disposition","attachment; filename="+file.getName());  
        return response.build();  
   
    }  
}