package com.sunglasses.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sunglasses.dao.ProductDao;
import com.sunglasses.dto.HashDto;
import com.sunglasses.dto.ImageDto;
import com.sunglasses.dto.ProductDto;

@Component
@Path("product")
public class ProductService {
	@Autowired
	private ProductDao productDao;
	
	@GET
	@Path("list")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ProductDto> getProducts() {
		 List<ProductDto> result = productDao.getProductList();
		 return result;
	}
	@GET
	@Path("{p_idx}/hash")
	@Produces({MediaType.APPLICATION_JSON})
	public List<HashDto> getHashs(@PathParam("p_idx") int p_idx) {
		List<HashDto> result = productDao.getHashList(p_idx);
		return result;
	}
	
	@GET
	@Path("{p_idx}/image")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ImageDto> getImages(@PathParam("p_idx") int p_idx) {
		List<ImageDto> result = productDao.getImageList(p_idx);
		return result;
	}
	
	@GET
	@Path("{p_idx}/model")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ImageDto> getModels(@PathParam("p_idx") int p_idx) {
		List<ImageDto> result = productDao.getModelList(p_idx);
		return result;
	}
	
	@GET
	@Path("{p_idx}/design")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ImageDto> getDesigns(@PathParam("p_idx") int p_idx) {
		List<ImageDto> result = productDao.getDesignList(p_idx);
		return result;
	}
	@GET
	@Path("{p_idx}/lens")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ImageDto> getLens(@PathParam("p_idx") int p_idx) {
		List<ImageDto> result = productDao.getLensList(p_idx);
		return result;
	}
//	@GET
//	@Path("{p_idx}")
//	@Produces({MediaType.APPLICATION_JSON})
//	public ProductDto getProduct(@PathParam("p_idx") int p_idx) {
//		ProductDto result = productDao.getProduct(p_idx);
//		return result;
//	}
//	
//	@POST
//	@Path("insertproduct")
//	@Produces({MediaType.APPLICATION_JSON})
//	@Consumes({MediaType.APPLICATION_JSON})
//	@Transactional
//	public Response insertProduct(ProductDto pd) {
//		System.out.println(pd.toJson().toString());
//		productDao.addProduct(pd);
//		productDao.addProductLog(new LogDto(0, pd.toJson().toString(), "productInsert", null));
//		//암호화
//		return Response.status(200).entity("A new product has been created").build();
//	}
}
