package com.sunglasses.dto;

public class HashDto {
	private int index, p_index;
	private String HashTAG;

	public HashDto(int index, String HashTAG, int p_index) {
			// TODO Auto-generated constructor stub
			this.index = index;
			this.HashTAG = HashTAG;
			this.p_index = p_index;
		}

	public HashDto() {
			// TODO Auto-generated constructor stub
		}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getHashTAG() {
		return HashTAG;
	}

	public void setHashTAG(String hashTAG) {
		HashTAG = hashTAG;
	}

	public int getP_index() {
		return p_index;
	}

	public void setP_index(int p_index) {
		this.p_index = p_index;
	}

}
