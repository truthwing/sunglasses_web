package com.sunglasses.dto;

public class ImageDto {
	private String image;
	private int p_index, index;
	public ImageDto() {
		// TODO Auto-generated constructor stub
	}
	public ImageDto(String image, int p_index, int index) {
		// TODO Auto-generated constructor stub
		this.image = image;this.p_index = p_index;this.index = index;
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getP_index() {
		return p_index;
	}
	public void setP_index(int p_index) {
		this.p_index = p_index;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
}
