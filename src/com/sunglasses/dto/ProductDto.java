package com.sunglasses.dto;

public class ProductDto {
	private int idx, p_price, opacity, flag, thumbs_up, gender;
	private String p_name, URL, b_icon_path, b_name, m_path;
	public ProductDto(int idx, int p_price, int opacity, String b_icon_path
			, int flag, int thumbs_up, String p_name, String URL, String b_name, String m_path, int gender) {
		// TODO Auto-generated constructor stub
		this.idx = idx;
		this.p_price = p_price;
		this.opacity = opacity;
		this.b_icon_path = b_icon_path;
		this.flag = flag;
		this.thumbs_up = thumbs_up;
		this.p_name = p_name;
		this.URL = URL;
		this.b_name = b_name;
		this.m_path = m_path;
		this.gender = gender;
	}
	public ProductDto() {
		// TODO Auto-generated constructor stub
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getP_price() {
		return p_price;
	}
	public void setP_price(int p_price) {
		this.p_price = p_price;
	}
	public int getOpacity() {
		return opacity;
	}
	public void setOpacity(int opacity) {
		this.opacity = opacity;
	}
	public String getB_icon_path() {
		return b_icon_path;
	}
	public void setB_icon_path(String b_icon_path) {
		this.b_icon_path = b_icon_path;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getThumbs_up() {
		return thumbs_up;
	}
	public void setThumbs_up(int thumbs_up) {
		this.thumbs_up = thumbs_up;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getB_name() {
		return b_name;
	}
	public void setB_name(String b_name) {
		this.b_name = b_name;
	}
	public String getM_path() {
		return m_path;
	}
	public void setM_path(String m_path) {
		this.m_path = m_path;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
}
