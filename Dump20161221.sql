-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: Sunglasses
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Brand`
--

DROP TABLE IF EXISTS `Brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brand` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `b_name` varchar(45) DEFAULT NULL COMMENT '브랜드 이름.',
  `b_icon_path` varchar(100) DEFAULT NULL COMMENT '브랜드 아이콘\n',
  PRIMARY KEY (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Brand`
--

LOCK TABLES `Brand` WRITE;
/*!40000 ALTER TABLE `Brand` DISABLE KEYS */;
INSERT INTO `Brand` VALUES (1,'[Louis Vuitton]','s_brand_logo2.png'),(2,'[PRADA]','s_brang_logo3.png'),(3,'[Gentle Monster]','s_brand_logo1.png');
/*!40000 ALTER TABLE `Brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Design`
--

DROP TABLE IF EXISTS `Design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Design` (
  `d_path` varchar(45) DEFAULT NULL COMMENT '안경테 디자인 이미지 경로\n',
  `p_index` int(11) NOT NULL COMMENT '제품 외부키\n',
  `dm_index` int(11) NOT NULL COMMENT '안경테 디자인 모델명 연결 외부키',
  PRIMARY KEY (`p_index`,`dm_index`),
  KEY `product_desing_idx_idx` (`p_index`),
  KEY `desing_model_idx_idx` (`dm_index`),
  CONSTRAINT `desing_model_idx` FOREIGN KEY (`dm_index`) REFERENCES `Design_Model` (`index`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `product_desing_idx` FOREIGN KEY (`p_index`) REFERENCES `Product` (`Idx`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Design`
--

LOCK TABLES `Design` WRITE;
/*!40000 ALTER TABLE `Design` DISABLE KEYS */;
/*!40000 ALTER TABLE `Design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Design_Model`
--

DROP TABLE IF EXISTS `Design_Model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Design_Model` (
  `index` int(11) NOT NULL,
  `dm_name` varchar(45) DEFAULT NULL COMMENT '안경테 디자인 이름',
  `dm_icon_path` varchar(45) DEFAULT NULL COMMENT '(배경) 디자인의 아이콘.',
  PRIMARY KEY (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Design_Model`
--

LOCK TABLES `Design_Model` WRITE;
/*!40000 ALTER TABLE `Design_Model` DISABLE KEYS */;
INSERT INTO `Design_Model` VALUES (1,'design1','s_design_1.png'),(2,'design2','s_design_2.png'),(3,'design3','s_design_3.png');
/*!40000 ALTER TABLE `Design_Model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hash`
--

DROP TABLE IF EXISTS `Hash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hash` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `HashTAG` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '해쉬태그',
  PRIMARY KEY (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hash`
--

LOCK TABLES `Hash` WRITE;
/*!40000 ALTER TABLE `Hash` DISABLE KEYS */;
INSERT INTO `Hash` VALUES (1,'아이유'),(2,'수지'),(3,'김태희'),(4,'하정우'),(5,'비'),(6,'예정화'),(7,'마동석'),(8,'이병헌');
/*!40000 ALTER TABLE `Hash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hash_Connect`
--

DROP TABLE IF EXISTS `Hash_Connect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hash_Connect` (
  `index` int(11) NOT NULL COMMENT '해쉬태그 외부키',
  `p_index` int(11) NOT NULL COMMENT '제품과 연결되는 외부키',
  PRIMARY KEY (`index`,`p_index`),
  KEY `product_idx_idx` (`p_index`),
  CONSTRAINT `hash_idx` FOREIGN KEY (`index`) REFERENCES `Hash` (`index`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `product_idx` FOREIGN KEY (`p_index`) REFERENCES `Product` (`Idx`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hash_Connect`
--

LOCK TABLES `Hash_Connect` WRITE;
/*!40000 ALTER TABLE `Hash_Connect` DISABLE KEYS */;
INSERT INTO `Hash_Connect` VALUES (1,1),(2,1),(3,1),(1,2),(3,2),(2,3),(3,4);
/*!40000 ALTER TABLE `Hash_Connect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Image` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `i_path` varchar(100) DEFAULT NULL COMMENT '기본 선그라스 이미지 경로',
  `p_index` int(11) DEFAULT NULL COMMENT '이미지와 연결된 제품',
  PRIMARY KEY (`index`),
  KEY `p_idx_idx` (`p_index`),
  CONSTRAINT `p_idx` FOREIGN KEY (`p_index`) REFERENCES `Product` (`Idx`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Image`
--

LOCK TABLES `Image` WRITE;
/*!40000 ALTER TABLE `Image` DISABLE KEYS */;
INSERT INTO `Image` VALUES (1,'p1_1.jpg',1),(2,'p1_2.jpg',1),(3,'p1_3.jpg',1),(4,'p1_4.jpg',1),(5,'p2_1.jpg',2),(6,'p2_2.jpg',2),(7,'p2_3.jpg',2),(8,'p2_4.jpg',2),(9,'p3_1.jpg',3),(10,'p3_2.jpg',3),(11,'p3_3.jpg',3),(12,'p3_4.jpg',3);
/*!40000 ALTER TABLE `Image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lens`
--

DROP TABLE IF EXISTS `Lens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lens` (
  `l_path` varchar(100) DEFAULT NULL COMMENT '렌즈 이미지 경로',
  `p_index` int(11) NOT NULL COMMENT '렌지 이미지와 연결된 제품.',
  `lm_index` int(11) NOT NULL COMMENT '렌즈 이미지 모델명과 연결된 외부키',
  PRIMARY KEY (`p_index`,`lm_index`),
  KEY `product_lens_idx_idx` (`p_index`),
  KEY `lens_model_idx_idx` (`lm_index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lens`
--

LOCK TABLES `Lens` WRITE;
/*!40000 ALTER TABLE `Lens` DISABLE KEYS */;
/*!40000 ALTER TABLE `Lens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lens_Model`
--

DROP TABLE IF EXISTS `Lens_Model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lens_Model` (
  `index` int(11) NOT NULL,
  `lm_name` varchar(45) DEFAULT NULL COMMENT '렌즈 모델명 이름.',
  `lm_icon_path` varchar(45) DEFAULT NULL COMMENT '렌즈모델 아이콘.',
  PRIMARY KEY (`index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lens_Model`
--

LOCK TABLES `Lens_Model` WRITE;
/*!40000 ALTER TABLE `Lens_Model` DISABLE KEYS */;
/*!40000 ALTER TABLE `Lens_Model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `Idx` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(45) DEFAULT NULL COMMENT '제품 이름\n',
  `p_price` int(11) DEFAULT NULL COMMENT '제품 가격\n',
  `opacity` int(11) DEFAULT NULL COMMENT '투명도에 따른 수치\n',
  `gender` int(11) DEFAULT NULL COMMENT '성별 0(무관),1(남),2(여)',
  `b_index` int(11) DEFAULT NULL COMMENT '브랜드와 연결된 외부키',
  `URL` varchar(200) DEFAULT NULL COMMENT '제품 상세정보 링크.',
  `flag` tinyint(4) DEFAULT NULL COMMENT '제품 노출 여부',
  `thumbs_up` tinyint(4) DEFAULT NULL COMMENT '추천제품.',
  PRIMARY KEY (`Idx`),
  KEY `brand_product_idx_idx` (`b_index`),
  CONSTRAINT `brand_product_idx` FOREIGN KEY (`b_index`) REFERENCES `Brand` (`index`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,'product1',20000,100,1,2,'http://www.naver.com',1,1),(2,'product2',30000,70,2,1,'http://www.daum.net',1,1),(3,'product3',40000,50,0,3,'http://www.google.com',1,1),(4,'product4',50000,0,1,2,'http://www.msn.co.kr',1,1);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `index` int(11) NOT NULL AUTO_INCREMENT,
  `m_path` varchar(100) DEFAULT NULL COMMENT '모델 이미지 사진 경로',
  `p_index` int(11) DEFAULT NULL COMMENT '모델과 연결된 제품 외부키',
  PRIMARY KEY (`index`),
  KEY `p_idx_idx` (`p_index`),
  CONSTRAINT `p_index` FOREIGN KEY (`p_index`) REFERENCES `Product` (`Idx`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (1,'p1_md1.jpg',1),(2,'p1_md2.jpg',1),(3,'p1_md3.jpg',1),(4,'p1_md4.jpg',1),(5,'p2_md1.jpg',2),(6,'p2_md2.jpg',2);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-21 17:02:35
